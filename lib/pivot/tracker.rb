module Pivot
  class Tracker
    def self.count(items)
      items.size
    end

    def self.item_for(items, assignee)
      items_for(items, assignee).last
    end

    def self.pivoted?(items, assignee)
      items_for(items, assignee).any?
    end

    def self.total_points(items, assignee: nil)
      return total_points_for_assignee(items, assignee) if assignee
      unique_total_points(items)
    end

    def self.unique_assignees(items)
      group_items(items).keys
    end

    def self.total_points_for_assignee(items, assignee)
      items_for(items, assignee).sum do |item|
        item[:points]
      end
    end

    def self.unique_total_points(items)
      group_items(items).map do |assignee, assigned_items|
        assigned_items.last
      end.sum do |item|
        item[:points]
      end
    end

    def self.items_for(items, assignee)
      group_items(items)[assignee] || []
    end

    def self.group_items(items)
      items.group_by do |item|
        item[:assignee]
      end
    end

    private_class_method :items_for
    private_class_method :group_items
    private_class_method :total_points_for_assignee
    private_class_method :unique_total_points
  end
end
